FROM jfloff/alpine-python:2.7
MAINTAINER endre.karlson@gmail.com

RUN apk --no-cache add git libffi-dev openssl-dev

COPY . /usr/src/app
WORKDIR /usr/src/app

RUN pip install pip -U
RUN pip install PyMySQL
RUN pip install .

RUN mkdir /config
VOLUME /config
