# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import logging

from gatekeeper.common import exception
from gatekeeper.db import api


LOG = logging.getLogger()


class Validator(object):
    def validate(self, credential_identifier, point_identifier):
        try:
            credential = api.find_credential(
                {"identifier": credential_identifier})
        except exception.NotFound:
            msg = "Credential %s sent by %s was not found"
            LOG.warn(msg, credential_identifier, point_identifier)
            return False

        try:
            point = api.find_point({"identifier": point_identifier})
        except exception.NotFound:
            msg = "Point %s requested by %s was not found"
            LOG.warn(msg, point_identifier, credential_identifier)
            return False

        if not credential.enabled:
            msg = "Disabled credential %s requested access to %s"
            LOG.warn(msg, credential.id, point.id)
            return False

        user = api.get_user(credential.user_id)
        if not user.enabled:
            msg = "Disabled user %s requested access %s"
            LOG.warn(msg, user.id, point.id)
            return False

        grant_criterion = {
            "point_id": point.id,
            "credential_id": credential.id
        }

        try:
            api.find_grant(grant_criterion)
        except exception.NotFound:
            LOG.warn("No grant found '%s' @ '%s'", credential.id, point.id)
            return False

        return True
