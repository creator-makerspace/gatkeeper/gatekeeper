# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import logging

import falcon
from oslo_serialization import jsonutils as json

from gatekeeper import access
from gatekeeper import events
from gatekeeper import particle_client


LOG = logging.getLogger(__name__)

DEFAULTS = {
    "particle_enable": False
}

_particle = None


def _get_particle(opts):
    global _particle
    if _particle is None:
        LOG.debug("Creating client")
        _particle = particle_client.ParticleClient(
            opts["client_id"], opts["client_secret"]
        )
    return _particle


class ParticleHookResource(object):
    def __init__(self, cfg):
        self.cfg = cfg

    def on_post(self, req, resp):
        verifier = access.Validator()
        event_emitter = events.get_emitter()

        require_token = self.cfg.get("particle_webhook_token", None)
        if require_token is None:
            err = {"error": "Token not configured, denied."}
            req.context["result"] = err
            return

        token = req.headers.get("X-AUTH-TOKEN")
        if token is None or token != require_token:
            req.status = falcon.HTTP_401
            err = {"error": "Invalid access token for Particle webhook"}
            req.context["result"] = err
            return

        doc = req.context["doc"]

        # Check if there's a 'data' key in the json from the hook
        if "data" not in doc:
            resp.json = {"error": "Missing data"}
            resp.status = falcon.HTTP_400
            return

        LOG.debug(doc)
        # Decode the nested json
        data = json.loads(doc["data"])
        LOG.info("Received webhook from Particle %s", doc)

        result = verifier.validate(data["credential_id"], doc["coreid"])

        event = {}
        event["device_id"] = doc["coreid"]
        event["point_id"] = doc["coreid"]
        event["credential_id"] = data["credential_id"]
        event["event"] = "credential.verify"
        event["state"] = "denied" if not result else "granted"

        msg = "Access %(state)s to %(credential_id)s " \
            "%(point_id)s@%(device_id)s"
        LOG.debug(msg % event)

        event_emitter.emit(event)

        client = _get_particle(self.cfg)

        response = "OPEN" if result else "DENIED"
        client.call_function(doc["coreid"], "command", response)

        req.context["result"] = result
