from gatekeeper.api import app as api_app
from gatekeeper.common import conf


conf.load_config(prefix="GK")
app = api_app.get_app()
