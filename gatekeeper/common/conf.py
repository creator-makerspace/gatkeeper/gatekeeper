# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import os

import toml


CFG = {}


def get_list_opt(name):
    data = CFG.get(name, None)
    if isinstance(data, str):
        data = data.split(",")
    return data


def get_env_config(prefix):
    """Return the env map of all possible vars prefixed with prefix_"""
    cfg = {}
    for k, v in os.environ.iteritems():
        if k.startswith("%s_" % prefix):
            k = k.replace("%s_" % prefix, "")
            cfg[k.lower()] = v
    return cfg


def load_config(defaults=None, prefix=None):
    """Get the apps config"""
    prefix = prefix or "APP"

    defaults = defaults or {}
    for k, v in defaults.items():
        i = CFG.get(k, None)
        if i is None:
            CFG[k] = v

    cfg_file = os.environ.get("GK_CFG_FILE")
    if cfg_file is not None and os.path.exists(cfg_file):
        with open(cfg_file) as toml_fh:
            toml_cfg = toml.loads(toml_fh.read())
            CFG.update(toml_cfg)

    env_cfg = {}

    flat_env_cfg = get_env_config(prefix.upper())
    for k, v in flat_env_cfg.items():
        env_cfg[k] = v

    CFG.update(env_cfg)


def set_defaults(opts):
    for key, value in opts.items():
        CFG.setdefault(key, value)
