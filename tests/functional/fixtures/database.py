# -*- coding: utf-8 -*-
# Copyright 2015 Hewlett-Packard Development Company, L.P.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import os
import shutil


from alembic.config import Config
from alembic import command
import fixtures

from gatekeeper.common import conf
from gatekeeper.db import models
from gatekeeper.db import session


class Database(fixtures.Fixture):

    def __init__(self, cfg, sqlite_db, sqlite_clean_db):
        self.cfg = cfg
        self.sqlite_db = sqlite_db
        self.sqlite_clean_db = sqlite_clean_db
        self.engine = session.get_engine()
        self.engine.dispose()
        conn = self.engine.connect()

        if cfg["db_url"] == "sqlite://":
            self.setup_sqlite()
        else:
            testdb = os.path.join(conf.CFG["state_path"], sqlite_db)
            self._migrate()
            if os.path.exists(testdb):
                return

        if cfg["db_url"] == "sqlite://":
            conn = self.engine.connect()
            self._DB = "".join(line for line in conn.connection.iterdump())
            self.engine.dispose()
        else:
            cleandb = os.path.join(conf.CFG["state_path"], sqlite_clean_db)
            shutil.copyfile(testdb, cleandb)

    def setUp(self):
        super(Database, self).setUp()

        if self.cfg["db_url"] == "sqlite://":
            conn = self.engine.connect()
            conn.connection.executescript(self._DB)
            self.addCleanup(self.engine.dispose)  # pylint: disable=E1101
        else:
            shutil.copyfile(
                os.path.join(conf.CFG["state_path"], self.sqlite_clean_db),
                os.path.join(conf.CFG["state_path"], self.sqlite_db),
            )

    def setup_sqlite(self):
        models.base.BASE.metadata.create_all(self.engine)
        self._migrate()

    def _migrate(self):
        alembic_path = os.path.join(
            os.path.dirname(__file__),
            '..', '..', '..', 'gatekeeper', 'db', 'alembic.ini')
        migration_path = os.path.join(
            os.path.dirname(__file__),
            '..', '..', '..', 'gatekeeper', 'db', 'alembic')

        alembic_cfg = Config(alembic_path)
        alembic_cfg.set_main_option('script_location', migration_path)

        with self.engine.begin() as con:
            alembic_cfg.attributes["connection"] = con
            command.stamp(alembic_cfg, "head")
