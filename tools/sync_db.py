# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import sys

from gatekeeper.common import conf
from gatekeeper.db import base
from gatekeeper.db import api
from gatekeeper.db import session


if __name__ == '__main__':
    conf.load_config(prefix="GK")
    engine = session.get_engine()
    base.BASE.metadata.drop_all(engine)
    base.BASE.metadata.create_all(engine)

    session = session.get_session()

    user = api.create_user({"name": "ekarlso"})

    credential = api.create_credential({
        "user_id": user.id,
        "identifier": "ccfbf859-cfce-4f98-9247-1fe9b7f2faac",
        "type": "nfc"
    })

    point = api.create_point({
        "name": "1f.door",
        "identifier": "mydoor"
    })

    api.create_grant({
        "credential_id": credential.id,
        "point_id": point.id
    })